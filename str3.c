#include <stdio.h>

#define FREQ ('z'-'a'+1)
#define LIMIT 500

int main(){
  int freq[FREQ];
  char text[LIMIT];
  scanf("%[^\n]",text);
  int i;
  for(i = 0; i < FREQ; i++)
    freq[i] = 0;
  i = 0;
  while(text[i]!='\0'){
    if (text[i] >= 'a' && text[i] <= 'z')
      freq[text[i] - 'a']++;
    else if (text[i] >= 'A' && text[i] <= 'Z')
      freq[text[i] - 'A']++;
    i++;
  }
  for(i = 0; i < FREQ; i++){
    if(freq[i]>0)
      printf("%c => %d\n", 'a'+i,freq[i]);
  }
  
  return 0;
}
