#include <stdio.h>
#include <stdlib.h>

typedef struct node{
  int value;
  struct node *next;
} llist;



int size_recursive(llist *l){
  if (l != NULL)
    return 1 + size_recursive(l->next);
  else
    return 0;
}

int size_iterative(llist *l){
  int size_l = 0;
  llist *p;
  for (p=l->next;p==NULL;p=l->next)
    size_l++;
  free(p);
  return size_l;
}

int altura(int e, llist* ll){
  llist * p = ll;
  int height = 0;
  int flag = 0;
  
  while(p!=NULL){
    if (flag == 0)
      if(p->value == e)
	flag = 1;
    if (flag == 1)
      height++;
    p = p->next;
  }
  free(p);
  return height;  
}

int deep(int e, llist* ll){
  llist* p = ll;
  int deep_el = 0;
  while (p != NULL){
    if (p->value == e)
      break;
    deep_el++;
    p = p->next;
  }
  if (p != NULL)
    return deep_el;
  else
    return 0;
}

int main(){
  llist *l = (llist*)malloc(sizeof(llist));
  l->value = 1;

  llist *p = (llist*)malloc(sizeof(llist));
  p->value = 4;
  l->next = p;

  printf("size l: %d \n",size_recursive(l));
  printf("Altura: %d\n",altura(4,l));
  printf("Deep: %d\n",deep(4,l));
  free(p);
  free(l);
  
  return 0;
}
