#include <stdio.h>
#include <stdlib.h>

char *ArChain(char c, int n){
  char *chain = (char *)malloc(n*sizeof(char));
  int i;
  for(i = 0; i < n; i++){
    chain[i] = c;
  }
  return chain;
}

int main(){

  char *chain = ArChain('c',5);

  printf("%s \n", chain);
  
  return 0;
}
