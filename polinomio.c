#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct _term{
  float coef;
  int exp;
  struct _term *next;
} term;

typedef term *Polinomio;

term *init_(){
  term *t = (term *)calloc(1,sizeof(term));
  if (t == NULL) exit(-1);
  return t;
}
// create NULL polinomio
Polinomio insertHead(){
  term *p = init_();
  p->exp = -1;
  p->coef = 0.0;
  p->next = p;
  return p;
}
void insertTerm(Polinomio pol,float coef, int exp){
  term *q, *aq, *x = init_();  
  x->coef = coef;
  x->exp = exp;

  aq = pol;
  q = pol->next;
  while (q->exp > exp){
    aq = q;
    q = q->next;
  }
  x->next = q;
  aq->next = x;
}
void printTerm(term *t){
  printf("%.2f*x^%d",t->coef, t->exp);
}
void printPolinomio(Polinomio pol){
  if (pol == NULL) return;
  //variable auxiliar  
  term *t;
  t = pol->next;
  if (t == pol) return;
  //print the first term
  printTerm(t);
  t = t->next;

  
  while (t != pol){
    printf(" + ");
    printTerm(t);
    t = t->next;
  }
  printf("\n");
}
Polinomio createPolinomio(char *expr){
  float coef;
  int r,n,exp,nn = 0;
  term *pol = insertHead();
  
  do{
    r = sscanf(expr + nn,"%f x^%d%n",&coef,&exp,&n);
    if(r == 0 || r == EOF) break;
    insertTerm(pol,coef,exp);
    nn += n;

    r = sscanf(expr+nn," + %n",&n);
    if(r == EOF) break;
    nn += n;
    
  }while(1);
  return pol;
}

float valor(Polinomio pol, double x){
  term *t = pol;
  term *q = t->next;
  float sum = 0;
  while (t != q){
    //exp = (double)q->exp;
    //printf("x: %.2lf ^ exp: %0.lf",x,exp);
    sum += q->coef * pow(x, q->exp);
    q = q->next;
  }

  return sum;
}
void freePolinomio(Polinomio pol){
  term *t = pol;
  term *q = t->next;
  term *tmp;
  while(t != q){
    tmp = q->next;
    free(q);
    q = tmp;
  }
  free(t);
}
Polinomio firstDerivate(Polinomio pol){
  term *t = pol;
  term *q = t->next;
  int exp;
  Polinomio first = insertHead();
  
  while(t != q){
    exp = q->exp;
    if(exp>0){//add term
      insertTerm(first,q->coef*exp,exp-1);
    }
    q = q->next;
  }
  
  return first;
}
Polinomio secondDerivate(Polinomio pol){
  term *t = pol;
  term *q = t->next;
  int exp;
  Polinomio second = insertHead();
  
  while(t != q){
    exp = q->exp;
    if(exp - 1 >0){//add term
      insertTerm(second,q->coef*exp*(exp-1),exp-2);
    }
    q = q->next;
  }
  
  return second;
}
void analizePolinomio(char *expr){
  float value,coef;
  int r,n,exp,nn = 0;
  term *pol = insertHead();

  r =sscanf(expr + nn, "%f %n",&value,&n);
  if (r == 0 || r == EOF) exit(-1);
  nn += n;
  
  do{   
    r = sscanf(expr + nn,"%f x^%d%n",&coef,&exp,&n);
    if(r == 0 || r == EOF) break;
    insertTerm(pol,coef,exp);
    nn += n;

    r = sscanf(expr+nn," + %n",&n);
    if(r == EOF) break;
    nn += n;
    
  }while(1);
  printf("P(%.2f): %.2f\n",value,valor(pol,value));
  Polinomio first = firstDerivate(pol);
  printPolinomio(first);
  Polinomio second = secondDerivate(pol);
  printPolinomio(second);
  
  freePolinomio(pol);
  freePolinomio(first);
  freePolinomio(second);
}
int main(){
  char exp[] = "3.0 2x^4 + 4x^2";
  analizePolinomio(exp);
  //Polinomio pol;
  //pol = createPolinomio(exp);
  //pol = insertHead();  
  //printf("%.2f*x^%d \n",pol->coef,pol->exp);
  //insertTerm(pol,4.8,3);
  //insertTerm(pol,3.2,1);
  //term *t = pol->next;
  //printf("%.2f*x^%d \n",t->coef,t->exp);
  //printPolinomio(pol);
  //printf("Value function: %.2f\n",valor(pol,3.0));
  //Polinomio first = firstDerivate(pol);
  //Polinomio second = secondDerivate(pol);
  
  //printPolinomio(second);
  //freePolinomio(first);
  //freePolinomio(second);
  //freePolinomio(pol);
  //printPolinomio(pol);
  return 0;
}
