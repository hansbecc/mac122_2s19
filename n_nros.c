#include <stdio.h>
#include <stdlib.h>

int main(){
  int *arNumbers;
  int n;
  printf("n: ");
  scanf("%d",&n);
  arNumbers = malloc(n*sizeof(int));
  int i,c;
  for(i = 0; i<n; i++)
    scanf("%d",&arNumbers[i]);
  i--;
  c = 0;
  int tmp;
  while (c < i){
    tmp = arNumbers[c];
    arNumbers[c] = arNumbers[i];
    arNumbers[i] = tmp;
    i--;
    c++;
  }
  for(i = 0; i < n; i++){
    printf("%d ",arNumbers[i]);
  }
  printf("\n");
  free(arNumbers);
  return 0;
}
