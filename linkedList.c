#include <stdio.h>
#include <stdlib.h>
/* typedef struct reg cell; */

/* struct reg{ */
/*   int content; */
/*   cell *next; */
/* }; */

typedef struct reg{
  int content;
  struct reg *next;
} cell;

void imprime (cell *ll){
  if (ll != NULL){
    printf("value: %d\n",ll->content);
    imprime(ll->next);
  }
  
}
void print(cell *ll){
  cell *p;
  for(p = ll;p != NULL;p = p->next)
    printf("value: %d\n",p->content);
}
int main(){
  cell *a = (cell *)malloc(sizeof(cell));
  a->content = 23;
  cell *b = (cell *)malloc(sizeof(cell));
  a->next = b;
  b->content = 27;
  cell *c = (cell *)malloc(sizeof(cell));
  b->next = c;
  c->content = 34;

  imprime(a);
  print(a);

  free(a);
  free(b);
  free(c);

  return 0;
}
