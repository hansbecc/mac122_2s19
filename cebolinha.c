#include <stdio.h>

int main(){
  int i = 0;
  char text[500];
  char c;
  //read text line
  scanf("%[^\n]",text);
  
  while(text[i] != '\0'){
    c = text[i];
    if ( ((c == 'r' || c == 'R') && text[i+1] != '\0') ||
	 (c == 'r' && text[i+1] == 'r') ||
	 (c == 'R' && text[i+1] == 'R') )
      text[i] = 'l';

    i++;	
  }
  printf("%s\n",text);
  return 0;
}
