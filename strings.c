#include <stdio.h>

int main(){
  int ch = getchar();
  do{
    while(ch == ' ')
      ch = getchar();
    
    while (ch != ' ' && ch != '\n' && ch != EOF){
      printf("%c",(char)ch);
      ch = getchar();
    }
    
    printf("\n");
    
  }while (ch != EOF && ch != '\n');
  return 0;
}
